package com.javatpoint;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.function.Function;
import java.util.stream.Collectors;

@Controller
public class DemoController {
    private Map<String, LinkedHashMap<String, String>> dropdownModel;

    public DemoController() {
        dropdownModel = new HashMap<String, LinkedHashMap<String, String>>();
        LinkedHashMap<String, String> map = new LinkedHashMap<String, String>();
        map.put("test1", "value1");
        map.put("test2", "value2");
        dropdownModel.put("main 1", map);
        
        map = new LinkedHashMap<String, String>();
        map.put("test-11", "value-11");
        map.put("test-12", "value-12");
        dropdownModel.put("main 2", map);
    }

    @RequestMapping("/")
    public String displayExample(FormData formData) {
        return "example";
    }


    @ModelAttribute("shapeTypes")
    public Map<String, String> getShapeTypes() {
        Map<String, String> shapeTypeMap = dropdownModel.keySet()
                .stream()
                .distinct()
                .sorted()
                .collect(Collectors.toMap(Function.identity(), Function.identity(),
                        (u, v) -> {
                            throw new IllegalStateException(String.format("Duplicate key %s", u));
                        },
                        LinkedHashMap::new));
        return shapeTypeMap;
    }

    @ModelAttribute("shapeSuggestionsJSON")
    public String getShapeSuggestionsJSON() throws JsonProcessingException {
        ObjectMapper mapper = new ObjectMapper();
        return mapper.writeValueAsString(dropdownModel);
    }
}
